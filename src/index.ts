import { Container } from 'typedi';
import { useContainer as useTypeormContainer } from 'typeorm';
import { useContainer  as useValidatorContainer } from 'class-validator';
import { FuelupApiService } from './services/fuelup-api.service';
import { logger } from './logger';
import { DataBase } from './database';
import { FuelsService } from './services/fuels.service';
import { PetrolStationsService } from './services/petrol-stations.service';
import { PromisePool } from '@supercharge/promise-pool';
import { config } from './config';


async function init() {
    try {
        if (config.database.poolSize <= 0) {
            throw new Error('Incorrect database pool size');
        }

        useTypeormContainer(Container);
        useValidatorContainer(Container);

        const database = Container.get(DataBase);
        const fuelupApiService = Container.get(FuelupApiService);
        const petrolStationsService = Container.get(PetrolStationsService);
        const fuelService = Container.get(FuelsService);

        await database.connect();


        const stations = await fuelupApiService.getPetrolStations();

        // Запускаем создание АЗС с ограничением количества промисов
        const { errors: createErrors } = await PromisePool
            .withConcurrency(config.database.poolSize)
            .for(stations)
            .process((dto) => petrolStationsService.createDeep(dto));

        for (const error of (createErrors || [])) {
            logger.error(error.message);
        }


        const prices = await fuelupApiService.getPrices();

        // Запускаем создание обновление цен с ограничением количества промисов
        const { errors: updateErrors } = await PromisePool
            .withConcurrency(config.database.poolSize)
            .for(prices)
            .process((dto) => {
                return fuelService.update({
                    Price: dto.Price,
                }, {
                    Id: dto.ProductId,
                    PetrolStationId: dto.StationId,
                }, true);
            });

        for (const error of (updateErrors || [])) {
            logger.error(error.message);
        }

        logger.info('exit');
    } catch (e) {
        logger.error(e);
    }
}

init();
