import {MigrationInterface, QueryRunner} from "typeorm";

export class init1654869335667 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "public"."loyalties" ("guid" uuid NOT NULL DEFAULT uuid_generate_v4(), "id" character varying NOT NULL, "petrol_station_id" uuid, CONSTRAINT "UQ_4393ac0305a1baf7a8da14045d8" UNIQUE ("guid"), CONSTRAINT "UQ_e9b4704e16cffa2a5b4d3a52ad7" UNIQUE ("id", "petrol_station_id"), CONSTRAINT "PK_4393ac0305a1baf7a8da14045d8" PRIMARY KEY ("guid"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."fuels_columns" ("guid" uuid NOT NULL DEFAULT uuid_generate_v4(), "external_column_id" character varying, "external_fuel_id" character varying, "column_id" uuid, "fuel_id" uuid, CONSTRAINT "UQ_97168a990c8ac1339ced6233704" UNIQUE ("guid"), CONSTRAINT "UQ_98f78a65af0477e6e52978064e0" UNIQUE ("column_id", "fuel_id"), CONSTRAINT "PK_97168a990c8ac1339ced6233704" PRIMARY KEY ("guid"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."fuels" ("guid" uuid NOT NULL DEFAULT uuid_generate_v4(), "id" character varying NOT NULL, "price" numeric NOT NULL, "type" character varying NOT NULL, "brand" character varying, "name" character varying NOT NULL, "petrol_station_id" uuid NOT NULL, CONSTRAINT "UQ_b0b5c94fdf48eca0d795b361a47" UNIQUE ("guid"), CONSTRAINT "UQ_36fc0d1fa68130df54ec70a6c58" UNIQUE ("id", "petrol_station_id"), CONSTRAINT "PK_b0b5c94fdf48eca0d795b361a47" PRIMARY KEY ("guid"))`, undefined);
        await queryRunner.query(`CREATE TYPE "public"."petrol_stations_take_off_mode_enum" AS ENUM('before', 'after', 'both')`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."petrol_stations" ("guid" uuid NOT NULL DEFAULT uuid_generate_v4(), "id" character varying NOT NULL, "name" character varying NOT NULL, "address" character varying NOT NULL, "brand" character varying, "location" jsonb NOT NULL, "take_off_before" boolean NOT NULL, "take_off_mode" "public"."petrol_stations_take_off_mode_enum", "post_pay" boolean NOT NULL, "enable" boolean NOT NULL, CONSTRAINT "UQ_5813214a1d173c83e4e8b2f14b3" UNIQUE ("guid"), CONSTRAINT "UQ_c5f1c03671df537b165b717742f" UNIQUE ("id"), CONSTRAINT "PK_5813214a1d173c83e4e8b2f14b3" PRIMARY KEY ("guid"))`, undefined);
        await queryRunner.query(`CREATE TABLE "public"."columns" ("guid" uuid NOT NULL DEFAULT uuid_generate_v4(), "n" character varying NOT NULL, "petrol_station_id" uuid NOT NULL, CONSTRAINT "UQ_3f07867d301f0f9626ffa56c14e" UNIQUE ("guid"), CONSTRAINT "UQ_c5bdf83e2970982b83e623773d0" UNIQUE ("n", "petrol_station_id"), CONSTRAINT "PK_3f07867d301f0f9626ffa56c14e" PRIMARY KEY ("guid"))`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."loyalties" ADD CONSTRAINT "FK_ebba01671a974cb6372f50b3435" FOREIGN KEY ("petrol_station_id") REFERENCES "public"."petrol_stations"("guid") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."fuels_columns" ADD CONSTRAINT "FK_42c066aafd54f3bed93c5c7f679" FOREIGN KEY ("column_id") REFERENCES "public"."columns"("guid") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."fuels_columns" ADD CONSTRAINT "FK_f813892b974992d754cbf65b8dc" FOREIGN KEY ("fuel_id") REFERENCES "public"."fuels"("guid") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."fuels" ADD CONSTRAINT "FK_ade1ff7e115cb38f0e6545671bf" FOREIGN KEY ("petrol_station_id") REFERENCES "public"."petrol_stations"("guid") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."columns" ADD CONSTRAINT "FK_adcd156bef5d4b9a0e7c8275194" FOREIGN KEY ("petrol_station_id") REFERENCES "public"."petrol_stations"("guid") ON DELETE NO ACTION ON UPDATE NO ACTION`, undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "public"."columns" DROP CONSTRAINT "FK_adcd156bef5d4b9a0e7c8275194"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."fuels" DROP CONSTRAINT "FK_ade1ff7e115cb38f0e6545671bf"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."fuels_columns" DROP CONSTRAINT "FK_f813892b974992d754cbf65b8dc"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."fuels_columns" DROP CONSTRAINT "FK_42c066aafd54f3bed93c5c7f679"`, undefined);
        await queryRunner.query(`ALTER TABLE "public"."loyalties" DROP CONSTRAINT "FK_ebba01671a974cb6372f50b3435"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."columns"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."petrol_stations"`, undefined);
        await queryRunner.query(`DROP TYPE "public"."petrol_stations_take_off_mode_enum"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."fuels"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."fuels_columns"`, undefined);
        await queryRunner.query(`DROP TABLE "public"."loyalties"`, undefined);
    }

}
