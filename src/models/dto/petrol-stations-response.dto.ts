import { IsBoolean, IsEnum, IsNotEmpty, IsNumber, IsOptional, IsString, ValidateNested } from 'class-validator';
import { IsNullOrString } from '../../helpers/class-validator';
import { Type } from 'class-transformer';
import { ETakeOffMode } from '../enums/ETakeOffMode';

// FIXME: Схема не полностью совпадает с документацией от 24.08.2021
//  https://drive.google.com/file/d/1oxKM_jEGDMYYCo2VGlIWa1z8uwo2K91g/view?usp=sharing
//  Loyalties обязательное в документации, но в ответе не приходит
//  TakeOffBefore обязательное в документации, но оно не описано
//  Columns.N обязательное в документации, но в ответе не приходит

class LocationDTO {
    /**
     * Широта
     */
    @IsNotEmpty()
    @IsNumber()
    public Lon: number;

    /**
     * Долгота
     */
    @IsNotEmpty()
    @IsNumber()
    public Lat: number;
}

/**
 * Номер ТРК на АЗС
 */
export type ColumnNumber = string;

export class ColumnDTO {
    /**
     * Номер ТРК
     */
    @IsOptional()
    @IsNumber()
    N: number | undefined;

    /**
     * Список доступных типов топлива на ТРК
     */
    @IsNotEmpty()
    @IsString({ each: true })
    Fuels: string[];
}

export class FuelDTO {
    /**
     * Уникальный идентификатор
     */
    @IsNotEmpty()
    @IsString()
    public Id: string;

    /**
     * Цена топлива
     */
    @IsNotEmpty()
    @IsNumber()
    public Price: number;

    /**
     * Тип топлива
     */
    @IsNotEmpty()
    @IsString()
    public Type: string;

    /**
     * Бренд топлива для брендированных видов, либо null для обычных
     */
    @IsOptional()
    @IsNullOrString()
    public Brand: string | null | undefined;

    /**
     * Название топлива как оно должно отображаться пользователю
     */
    @IsNotEmpty()
    @IsString()
    public Name: string;
}

export class PetrolStationsResponseDTO {
    /**
     * Уникальный идентификатор станции АЗС
     */
    @IsNotEmpty()
    @IsString()
    public Id: string;

    /**
     * Наименование станции
     */
    @IsNotEmpty()
    @IsString()
    public Name: string;

    /**
     * Адрес станции
     */
    @IsNotEmpty()
    @IsString()
    public Address: string;

    /**
     * Бренд, под которым работает АЗС
     */
    @IsOptional()
    @IsNullOrString()
    public Brand: string | null | undefined;

    /**
     * Гео-координаты станции
     */
    @IsNotEmpty()
    @ValidateNested()
    @Type(() => LocationDTO)
    public Location: LocationDTO;

    @IsNotEmpty()
    @IsBoolean()
    public TakeOffBefore: boolean;

    /**
     * Момент снятия пистолета:
     * до создания заказа (before), после (after) или без разницы (both)
     */
    @IsOptional()
    @IsEnum(ETakeOffMode)
    public TakeOffMode: ETakeOffMode | undefined;

    /**
     * АЗС работает по предоплате (false) или по постоплате (true)
     */
    @IsNotEmpty()
    @IsBoolean()
    public PostPay: boolean;

    /**
     * Статус станции: true - доступна, false - выключена
     */
    @IsNotEmpty()
    @IsBoolean()
    public Enable: boolean;

    /**
     * Список ТРК на АЗС
     */
    @IsOptional()
    @ValidateNested({ each: true })
    @Type(() => ColumnDTO)
    public Columns: Map<ColumnNumber, ColumnDTO> | null;


    @IsOptional()
    @ValidateNested({ each: true })
    @Type(() => FuelDTO)
    public Fuels: Array<FuelDTO> | null;

    /**
     * Список программ лояльности, доступных на АЗС
     */
    @IsOptional()
    @IsString({ each: true })
    public Loyalties: string[] | undefined;
}
