import { IsNotEmpty, IsNumber, IsString, Matches } from 'class-validator';

// FIXME: Схема не полностью совпадает с документацией от 24.08.2021
//  https://drive.google.com/file/d/1oxKM_jEGDMYYCo2VGlIWa1z8uwo2K91g/view?usp=sharing
//  ProductName обязательное в документации, но оно не описано и не приходит в ответе
//  В документации название поля StationID, но приходит StationId

/**
 * Цены на топливо
 */
export class PricesResponseDTO {
    /**
     * Уникальный идентификатор АЗС в системе FuelUp
     */
    @IsNotEmpty()
    @IsString()
    @Matches('^[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}$')
    public StationId: string;

    /**
     * Идентификатор типа топлива
     */
    @IsNotEmpty()
    @IsString()
    public ProductId: string;

    /**
     * Цена топлива в рублях
     */
    @IsNotEmpty()
    @IsNumber()
    public Price: number;
}
