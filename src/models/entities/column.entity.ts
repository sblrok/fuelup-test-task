import {
    BaseEntity,
    Column,
    Entity, ManyToOne, OneToMany, Unique,
} from 'typeorm';
import { PetrolStationEntity } from './petrol-station.entity';
import { JoinColumn } from 'typeorm';
import { FuelsColumnsEntity } from './fuels-columns.entity';

/**
 * ТРК
 */
@Entity({ name: 'columns', schema: 'public' })
@Unique(['N', 'PetrolStationId'])
export class ColumnEntity extends BaseEntity {

    @Column('uuid', {
        comment: 'Уникальный ID',
        generated: 'uuid',
        primary: true,
        unique: true,
        nullable: false,
        name: 'guid',
    })
    public guid: string;

    @Column('varchar', {
        comment: 'Номер ТРК',
        nullable: false,
        name: 'n'
    })
    public N: string;

    @Column('uuid', {
        comment: 'К какой АЗС относится эта колонка',
        nullable: false,
        name: 'petrol_station_id',
    })
    public PetrolStationId: string;

    @ManyToOne(() => PetrolStationEntity, petrolStation => petrolStation.Columns)
    @JoinColumn({ name: 'petrol_station_id' })
    public PetrolStation: PetrolStationEntity;

    @OneToMany(() => FuelsColumnsEntity, fuelsColumns => fuelsColumns.Column)
    public FuelsColumns: FuelsColumnsEntity[];
}
