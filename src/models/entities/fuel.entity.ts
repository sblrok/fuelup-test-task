import {
    BaseEntity,
    Column,
    Entity, ManyToOne, OneToMany, Unique,
} from 'typeorm';
import { PetrolStationEntity } from './petrol-station.entity';
import { JoinColumn } from 'typeorm';
import { FuelsColumnsEntity } from './fuels-columns.entity';


/**
 * Топливо
 */
@Entity({ name: 'fuels', schema: 'public' })
@Unique(['Id', 'PetrolStationId'])
export class FuelEntity extends BaseEntity {

    @Column('uuid', {
        comment: 'Уникальный ID',
        generated: 'uuid',
        primary: true,
        unique: true,
        nullable: false,
        name: 'guid',
    })
    public guid: string;

    @Column('varchar', {
        comment: 'Внешний никальный идентификатор',
        nullable: false,
        name: 'id'
    })
    public Id: string;

    @Column('numeric', {
        comment: 'Цена топлива',
        nullable: false,
        name: 'price'
    })
    public Price: number;

    @Column('varchar', {
        comment: 'Цена топлива',
        nullable: false,
        name: 'type'
    })
    public Type: string;

    @Column('varchar', {
        comment: 'Бренд топлива для брендированных видов, либо null для обычных',
        nullable: true,
        name: 'brand'
    })
    public Brand: string | null;

    @Column('varchar', {
        comment: 'Название топлива как оно должно отображаться пользователю',
        nullable: false,
        name: 'name'
    })
    public Name: string;

    @Column('uuid', {
        comment: 'К какой АЗС относится это топливо',
        nullable: false,
        name: 'petrol_station_id',
    })
    public PetrolStationId: string;

    @ManyToOne(() => PetrolStationEntity, petrolStation => petrolStation.Fuels)
    @JoinColumn({ name: 'petrol_station_id' })
    public PetrolStation: PetrolStationEntity;

    @OneToMany(() => FuelsColumnsEntity, fuelsColumns => fuelsColumns.Fuel)
    public FuelsColumns: FuelsColumnsEntity[];
}
