import {
    BaseEntity,
    Column,
    Entity, ManyToOne, Unique,
} from 'typeorm';
import { ColumnEntity } from './column.entity';
import { JoinColumn } from 'typeorm/';
import { FuelEntity } from './fuel.entity';


/**
 * Топливо
 */
@Entity({ name: 'fuels_columns', schema: 'public' })
@Unique(['ColumnId', 'FuelId'])
export class FuelsColumnsEntity extends BaseEntity {

    @Column('uuid', {
        comment: 'Уникальный ID',
        generated: 'uuid',
        primary: true,
        unique: true,
        nullable: false,
        name: 'guid',
    })
    public guid: string;

    @Column('varchar', {
        comment: 'Внешний уникальный идентификатор колонки',
        nullable: true,
        name: 'external_column_id'
    })
    public ExternalColumnId: string | null;

    @Column('varchar', {
        comment: 'Внешний уникальный идентификатор топлива',
        nullable: true,
        name: 'external_fuel_id'
    })
    public ExternalFuelId: string | null;

    @Column('uuid', {
        comment: 'Внутренний уникальный идентификатор колонки',
        nullable: true,
        name: 'column_id'
    })
    public ColumnId: string | null;

    @ManyToOne(() => ColumnEntity, column => column.FuelsColumns)
    @JoinColumn({ name: 'column_id' })
    public Column: ColumnEntity | null;

    @Column('uuid', {
        comment: 'Внутренний уникальный идентификатор топлива',
        nullable: true,
        name: 'fuel_id'
    })
    public FuelId: string | null;

    @ManyToOne(() => FuelEntity, fuel => fuel.FuelsColumns)
    @JoinColumn({ name: 'fuel_id' })
    public Fuel: FuelEntity | null;
}
