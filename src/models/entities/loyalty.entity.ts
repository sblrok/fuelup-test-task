import {
    BaseEntity,
    Column,
    Entity, ManyToOne, Unique,
} from 'typeorm';
import { PetrolStationEntity } from './petrol-station.entity';
import { JoinColumn } from 'typeorm';

/**
 * Список программ лояльности, доступных на АЗС
 */
@Entity({ name: 'loyalties', schema: 'public' })
@Unique(['Id', 'PetrolStationId'])
export class LoyaltyEntity extends BaseEntity {

    @Column('uuid', {
        comment: 'Уникальный ID',
        generated: 'uuid',
        primary: true,
        unique: true,
        nullable: false,
        name: 'guid',
    })
    public guid: string;

    @Column('varchar', {
        comment: 'ID программы лояльности',
        nullable: false,
        name: 'id'
    })
    public Id: string;

    @Column('uuid', {
        comment: 'К какой АЗС относится эта программа лояльности',
        nullable: true,
        name: 'petrol_station_id',
    })
    public PetrolStationId: string | null;

    @ManyToOne(() => PetrolStationEntity, petrolStation => petrolStation.Loyalties)
    @JoinColumn({ name: 'petrol_station_id' })
    public PetrolStation: PetrolStationEntity | null;
}
