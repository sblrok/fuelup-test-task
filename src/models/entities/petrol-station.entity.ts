import {
    BaseEntity,
    Column,
    Entity, OneToMany,
} from 'typeorm';
import { Location } from '../interfaces/location';
import { ColumnEntity } from './column.entity';
import { LoyaltyEntity } from './loyalty.entity';
import { ETakeOffMode } from '../enums/ETakeOffMode';
import { FuelEntity } from './fuel.entity';


/**
 * АЗС
 */
@Entity({ name: 'petrol_stations', schema: 'public' })
export class PetrolStationEntity extends BaseEntity {

    @Column('uuid', {
        comment: 'Уникальный ID',
        generated: 'uuid',
        primary: true,
        unique: true,
        nullable: false,
        name: 'guid',
    })
    public guid: string;

    @Column('varchar', {
        comment: 'Внешний никальный идентификатор',
        nullable: false,
        unique: true,
        name: 'id'
    })
    public Id: string;

    @Column('varchar', {
        comment: 'Наименование станции',
        nullable: false,
        name: 'name'
    })
    public Name: string;

    @Column('varchar', {
        comment: 'Адрес станции',
        nullable: false,
        name: 'address'
    })
    public Address: string;

    @Column('varchar', {
        comment: 'Бренд, под которым работает АЗС',
        nullable: true,
        name: 'brand'
    })
    public Brand: string | null;

    // Можно использовать geometryjs или вынести в отдельную таблицу
    @Column('jsonb', {
        comment: 'Гео-координаты станции',
        nullable: false,
        name: 'location'
    })
    public Location: Location;

    @Column('boolean', {
        nullable: false,
        name: 'take_off_before'
    })
    public TakeOffBefore: boolean;

    @Column('enum', {
        enum: ETakeOffMode,
        comment: 'Момент снятия пистолета',
        nullable: true,
        name: 'take_off_mode'
    })
    public TakeOffMode: ETakeOffMode;

    @Column('boolean', {
        comment: 'АЗС работает по предоплате (false) или по постоплате (true)',
        nullable: false,
        name: 'post_pay'
    })
    public PostPay: boolean;

    @Column('boolean', {
        comment: 'Статус станции: true - доступна, false - выключена',
        nullable: false,
        name: 'enable'
    })
    public Enable: boolean;
    
    @OneToMany(() => ColumnEntity, column => column.PetrolStation)
    public Columns: ColumnEntity[];

    @OneToMany(() => LoyaltyEntity, loyalty => loyalty.PetrolStation)
    public Loyalties: LoyaltyEntity[];

    @OneToMany(() => FuelEntity, fuel => fuel.PetrolStation)
    public Fuels: FuelEntity[];
}
