export enum ETakeOffMode {
    Before = 'before',
    After = 'after',
    Both = 'both',
}
