import { IFuel } from './fuel';
import { IPetrolStation } from './petrol-station';

export interface IColumn {
    guid?: string;
    N: string;
    PetrolStationId: string;
    PetrolStation?: IPetrolStation;
    Fuels?: IFuel[];
}
