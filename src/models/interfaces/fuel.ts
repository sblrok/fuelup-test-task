import { IColumn } from './column';
import { IPetrolStation } from './petrol-station';

export interface IFuel {
    guid?: string;
    Id: string;
    Price: number;
    Type: string;
    Brand: string | null;
    Name: string;
    PetrolStationId: string;
    PetrolStation?: IPetrolStation;
    Columns?: IColumn[];
}
