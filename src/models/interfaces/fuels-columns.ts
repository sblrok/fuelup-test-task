import { IColumn } from './column';
import { IFuel } from './fuel';

export interface IFuelsColumns {
    guid?: string;
    ExternalColumnId: string | null;
    ExternalFuelId: string | null;
    ColumnId: string | null;
    FuelId: string | null;
    Column?: IColumn | null;
    Fuel?: IFuel | null;
}
