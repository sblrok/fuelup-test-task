export interface Location {
    Lon: number;
    Lat: number;
}
