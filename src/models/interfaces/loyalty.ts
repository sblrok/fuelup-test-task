import { IPetrolStation } from './petrol-station';

export interface ILoyalty {
    guid?: string;
    Id: string;
    PetrolStationId: string | null;
    PetrolStation?: IPetrolStation;
}
