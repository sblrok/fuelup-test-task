import { IColumn } from './column';
import { ILoyalty } from './loyalty';
import { Location } from './location';
import { ETakeOffMode } from '../enums/ETakeOffMode';

export interface IPetrolStation {
    guid?: string;
    Id: string;
    Name: string;
    Address: string;
    Brand: string | null;
    Location: Location;
    TakeOffBefore: boolean;
    TakeOffMode?: ETakeOffMode;
    PostPay: boolean;
    Enable: boolean;
    Columns?: IColumn[];
    Loyalties?: ILoyalty[];
}
