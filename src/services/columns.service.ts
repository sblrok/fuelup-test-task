import { Service } from 'typedi';
import { AllowTransactions, UseTransaction } from './middlewares/transaction.middleware';
import { EntityManager } from 'typeorm';
import { uuid } from 'uuidv4'
import { IColumn } from '../models/interfaces/column';
import { ColumnEntity } from '../models/entities/column.entity';

type CreateOptions = Omit<IColumn, 'PetrolStation' | 'Fuels'>;

@Service()
@AllowTransactions
export class ColumnsService {

    @UseTransaction()
    public async create(column: CreateOptions, transaction?: EntityManager) {
        column.guid = column.guid || uuid();

        const repository = transaction.getRepository(ColumnEntity);
        await repository.insert({
            guid: column.guid,
            N: column.N,
            PetrolStationId: column.PetrolStationId,
        });

        return column.guid;
    }

}
