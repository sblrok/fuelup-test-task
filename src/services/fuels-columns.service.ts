import { Service } from 'typedi';
import { AllowTransactions, UseTransaction } from './middlewares/transaction.middleware';
import { EntityManager } from 'typeorm';
import { uuid } from 'uuidv4'
import { FuelsColumnsEntity } from '../models/entities/fuels-columns.entity';
import { IFuelsColumns } from '../models/interfaces/fuels-columns';

type CreateOptions = Omit<IFuelsColumns, 'Column' | 'Fuel'>;

@Service()
@AllowTransactions
export class FuelsColumnsService {
    @UseTransaction()
    public async create(fuelsColumns: CreateOptions, transaction?: EntityManager) {
        fuelsColumns.guid = fuelsColumns.guid || uuid();

        const repository = transaction.getRepository(FuelsColumnsEntity);
        await repository.insert({
            guid: fuelsColumns.guid,
            ExternalColumnId: fuelsColumns.ExternalColumnId,
            ExternalFuelId: fuelsColumns.ExternalFuelId,
            ColumnId: fuelsColumns.ColumnId,
            FuelId: fuelsColumns.FuelId,
        });

        return fuelsColumns.guid;
    }
}
