import { Service } from 'typedi';
import { AllowTransactions, UseTransaction } from './middlewares/transaction.middleware';
import { EntityManager } from 'typeorm';
import { IFuel } from '../models/interfaces/fuel';
import { FuelEntity } from '../models/entities/fuel.entity';
import { uuid } from 'uuidv4'
import { FindConditions } from 'typeorm/find-options/FindConditions';

type CreateOptions = Omit<IFuel, 'PetrolStation' | 'Columns'>;
type UpdateOptions = Partial<CreateOptions>;

@Service()
@AllowTransactions
export class FuelsService {

    @UseTransaction()
    public async create(fuel: CreateOptions, transaction?: EntityManager) {
        fuel.guid = fuel.guid || uuid();

        const repository = transaction.getRepository(FuelEntity);
        await repository.insert({
            guid: fuel.guid,
            Id: fuel.Id,
            Price: fuel.Price,
            Type: fuel.Type,
            Brand: fuel.Brand,
            Name: fuel.Name,
            PetrolStationId: fuel.PetrolStationId,
        });

        return fuel.guid;
    }

    @UseTransaction()
    public async update(fuel: UpdateOptions, options: FindConditions<UpdateOptions>, safe = false, transaction?: EntityManager) {
        const repository = transaction.getRepository(FuelEntity);

        if (safe) {
            const existed = await repository.findOne(options);
            if (!existed) {
                throw new Error(`Топливо по условиям ${JSON.stringify(options)} не найдено!`);
            }
        }

        await repository.update(options, fuel);
    }

}
