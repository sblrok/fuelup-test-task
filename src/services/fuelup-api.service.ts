import { Service } from 'typedi';
import axios from 'axios';
import { config } from '../config';
import { transformAndValidate } from 'class-transformer-validator';
import { PricesResponseDTO, PetrolStationsResponseDTO } from '../models/dto';

@Service()
export class FuelupApiService {

    public async getPetrolStations(): Promise<PetrolStationsResponseDTO[]> {
        const response = await axios.get<PetrolStationsResponseDTO[]>(`${config.fuelup.url}/station?apikey=${config.fuelup.apikey}`);
        return transformAndValidate(PetrolStationsResponseDTO, response.data);
    }

    public async getPrices(): Promise<PricesResponseDTO[]> {
        const response = await axios.get<PricesResponseDTO[]>(`${config.fuelup.url}/price?apikey=${config.fuelup.apikey}`);
        return transformAndValidate(PricesResponseDTO, response.data);
    }
}
