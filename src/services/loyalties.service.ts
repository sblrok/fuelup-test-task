import { Service } from 'typedi';
import { AllowTransactions, UseTransaction } from './middlewares/transaction.middleware';
import { EntityManager } from 'typeorm';
import { uuid } from 'uuidv4'
import { ILoyalty } from '../models/interfaces/loyalty';
import { LoyaltyEntity } from '../models/entities/loyalty.entity';

type CreateOptions = Omit<ILoyalty, 'PetrolStation'>;

@Service()
@AllowTransactions
export class LoyaltiesService {

    @UseTransaction()
    public async create(loyalty: CreateOptions, transaction?: EntityManager) {
        loyalty.guid = loyalty.guid || uuid();

        const repository = transaction.getRepository(LoyaltyEntity);
        await repository.insert({
            guid: loyalty.guid,
            Id: loyalty.Id,
            PetrolStationId: loyalty.PetrolStationId,
        });

        return loyalty.guid;
    }

}
