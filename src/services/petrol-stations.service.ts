import { Service } from 'typedi';
import { AllowTransactions, UseTransaction } from './middlewares/transaction.middleware';
import { EntityManager } from 'typeorm';
import { uuid } from 'uuidv4'
import { IPetrolStation } from '../models/interfaces/petrol-station';
import { PetrolStationEntity } from '../models/entities/petrol-station.entity';
import { ColumnDTO, PetrolStationsResponseDTO } from '../models/dto';
import { FuelsService } from './fuels.service';
import { LoyaltiesService } from './loyalties.service';
import { ColumnsService } from './columns.service';
import { FuelsColumnsService } from './fuels-columns.service';
import { logger } from '../logger';

type CreateOptions = Omit<IPetrolStation, 'Columns' | 'Loyalties'>;

@Service()
@AllowTransactions
export class PetrolStationsService {
    constructor(
        private loyaltiesService: LoyaltiesService,
        private fuelsService: FuelsService,
        private columnsService: ColumnsService,
        private fuelsColumnsService: FuelsColumnsService,
    ) {}

    @UseTransaction()
    public async create(petrolStation: CreateOptions, transaction?: EntityManager) {
        petrolStation.guid = petrolStation.guid || uuid();

        const repository = transaction.getRepository(PetrolStationEntity);
        await repository.insert({
            guid: petrolStation.guid,
            Id: petrolStation.Id,
            Name: petrolStation.Name,
            Address: petrolStation.Address,
            Brand: petrolStation.Brand,
            Location: petrolStation.Location,
            TakeOffBefore: petrolStation.TakeOffBefore,
            TakeOffMode: petrolStation.TakeOffMode,
            PostPay: petrolStation.PostPay,
            Enable: petrolStation.Enable,
        });

        return petrolStation.guid;
    }

    /**
     * Создает PetrolStation и зависимости
     * @param dto
     * @param transaction
     */
    @UseTransaction()
    public async createDeep(dto: PetrolStationsResponseDTO, transaction?: EntityManager) {
        const petrolStationId = await this.create(dto, transaction);

        // Создаем Loyalties
        if (!dto.Loyalties || dto.Loyalties.length === 0) {
            logger.warn(`По АЗС ${dto.Id} нет данных о программах лояльности!`);
        }

        for (const loyaltyId of (dto.Loyalties || [])) {
            await this.loyaltiesService.create({
                Id: loyaltyId,
                PetrolStationId: petrolStationId,
            }, transaction);
        }

        // Создаем Fuels
        if (!dto.Fuels || dto.Fuels.length === 0) {
            logger.warn(`По АЗС ${dto.Id} нет данных о видах топлива!`);
        }

        const fuelsTable = {};
        for (const fuel of (dto.Fuels || [])) {
            const guid = await this.fuelsService.create({
                ...fuel,
                PetrolStationId: petrolStationId,
            }, transaction);
            fuelsTable[fuel.Id] = guid;
        }

        // Создаем Columns
        if (!dto.Columns || dto.Columns.size === 0) {
            logger.warn(`По АЗС ${dto.Id} нет данных о ТРК!`);
        }

        for (const columnId of (dto.Columns?.keys() || [])) {
            const guid = await this.columnsService.create({
                N: columnId,
                PetrolStationId: petrolStationId,
            }, transaction);

            const column: ColumnDTO = dto.Columns.get(columnId);

            // Создаем связь Fuels - Columns
            if (!column.Fuels || column.Fuels.length === 0) {
                logger.warn(`У АЗС ${dto.Id} не указана связь топлива и колонок!`);
            }
            
            for (const fuelId of (column.Fuels || [])) {
                await this.fuelsColumnsService.create({
                    ExternalColumnId: columnId,
                    ExternalFuelId: fuelId,
                    ColumnId: guid,
                    FuelId: fuelsTable[fuelId],
                }, transaction);
            }
        }

        logger.info(`АЗС ${dto.Id} создана!`);

        return petrolStationId;
    }
}
